import React from "react";
import "./Home.css";
import PrimeImg from "./images/prime.jpg";
import Product from "./Product";
function Home() {
  return (
    <div className="home">
      <div className="home__container">
        <img className="home__image" src={PrimeImg} alt="" />
      </div>

      <div className="home__row">
        <Product
          id={1}
          title="Wings of Fire: An Autobiography of Abdul Kalam Paperback – 1 January 1999"
          image="https://images-na.ssl-images-amazon.com/images/I/51+2bxo4TuL._SX325_BO1,204,203,200_.jpg"
          price={119}
          rating={5}
        />
        <Product
          id={2}
          title="you are the Best Wife: A True Love Story"
          image="https://m.media-amazon.com/images/I/81tXACyDgBL._AC_UY327_FMwebp_QL65_.jpg"
          price={119}
          rating={5}
        />
      </div>

      <div className="home__row">
        <Product
          id={3}
          title="Redmi Band (Direct USB Charging, 5ATM Water Resistant, Full Touch Color Display, App Notifications, Music Control)"
          image="https://m.media-amazon.com/images/I/71RrPTCdy-L._AC_UL480_FMwebp_QL65_.jpg"
          price={1599}
          rating={4}
        />
        <Product
          id={4}
          title="HONOR Band 5 (MeteoriteBlack)- Waterproof Full Color AMOLED Touchscreen, SpO2 (Blood Oxygen), Music Control, Watch Faces Store, up to 14 Day Battery Life"
          image="https://m.media-amazon.com/images/I/81CwFEsrQUL._AC_UL480_FMwebp_QL65_.jpg"
          price={2199}
          rating={5}
        />
        <Product
          id={5}
          title="Redmi Earbuds S, Punchier Sound,Up to 12 Hours of Playback time, IPX4 Sweat & Splash Proof& DSP Environmental Noise Cancellation"
          image="https://m.media-amazon.com/images/I/61k1jK9XwVL._AC_UY327_FMwebp_QL65_.jpg"
          price={2999}
          rating={5}
        />

        <Product
          id={7}
          title="Mi True Wireless Earphones 2 with Balanced Sound,14 hrs Battery Life; 14.2 mm Dynamic Driver, Dual Mic Environment Noise Cancellation, One Step Pairing, Smart in-Ear Detection, LHDC Audio Codec"
          image="https://m.media-amazon.com/images/I/614imrpvD9L._AC_UY327_FMwebp_QL65_.jpg"
          price={3999}
          rating={5}
        />

        <Product
          id={8}
          title="Mi 10000mAH Li-Polymer Power Bank 2i (Black) with 18W Fast Charging"
          image="https://m.media-amazon.com/images/I/619AI9CLXTL._AC_UY327_FMwebp_QL65_.jpg"
          price={899}
          rating={4}
        />
      </div>

      <div className="home__row">
        <Product
          key={9}
          id={6}
          title="Mi Corded & Cordless Waterproof Beard Trimmer with Fast Charging - 40 length settings"
          image="https://m.media-amazon.com/images/I/617J2pRxZ+L._AC_UL480_FMwebp_QL65_.jpg"
          price={1499}
          rating={5}
        />
      </div>
    </div>
  );
}

export default Home;
