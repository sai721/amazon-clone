import React from "react";
import "./Checkout.css";
import Subtotal from "./Subtotal";
import { useStateValue } from "./StateProvider";
import Cart from "./Cart";
function Checkout() {
  const [{ basket }, dispatch] = useStateValue();

  return (
    <div className="checkout">
      <div className="checkout__left">
        <img
          className="checkout__ad"
          src="https://gos3.ibcdn.com/top-1568020025.jpg"
          alt=""
        />

        <div>
          <h2 className="checkout__title">Your shopping cart</h2>
          {basket.map((item) => (
            <Cart item={item} />
          ))}
        </div>
      </div>

      <div className="checkout__right">
        <Subtotal />
      </div>
    </div>
  );
}

export default Checkout;
