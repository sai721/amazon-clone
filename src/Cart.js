import React from "react";
import "./Cart.css";
import { useStateValue } from "./StateProvider";
function Cart({ item }) {
  const [{ basket }, dispatch] = useStateValue();

  //   const removeFromBasket = (id) => {
  //     dispatch({
  //       type: "REMOVE_FROM_BASKET" ,
  //       payload: id,
  //     });
  //     console.log(basket);
  //   };
  return (
    <div>
      <div className="cart__basket">
        <img src={item.image} alt="" className="cart__image" />
        <div className="cart__info">
          <div className="cart_ptags">
            <p className="cart__title">{item.title}</p>
            <div className="product__rating">
              {Array(item.rating)
                .fill()
                .map((_, i) => (
                  <p>⭐</p>
                ))}
            </div>
            <p className="cart__price">{item.price}</p>
          </div>

          <button className="cart__button">Remove</button>
        </div>
      </div>
    </div>
  );
}

export default Cart;
